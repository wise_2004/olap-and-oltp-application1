
--Retrieve the total sales amount for each product category for a specific time period
SELECT p.prod_category, SUM(s.amount_sold) AS total_sales
FROM sh.sales s
JOIN sh.products p ON s.prod_id = p.prod_id
JOIN sh.times t ON s.time_id = t.time_id
WHERE t.time_id BETWEEN '1998-01-01' AND '2000-12-31'  -- replace with actual dates
GROUP BY p.prod_category;

-- Calculate the average sales quantity by region for a particular product
SELECT c.country_region, AVG(s.quantity_sold) AS avg_quantity_sold
FROM sh.sales s
JOIN sh.customers cu ON s.cust_id = cu.cust_id
JOIN sh.countries c ON cu.country_id = c.country_id
WHERE s.prod_id = 40 -- replace 40 with an actual integer product ID
GROUP BY c.country_region;

-- Find the top five customers with the highest total sales amount
SELECT s.cust_id, SUM(s.amount_sold) AS total_sales
FROM sh.sales s
GROUP BY s.cust_id
ORDER BY total_sales DESC
LIMIT 5;


